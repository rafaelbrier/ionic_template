import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RunBookPage } from './run-book/run-book.page';

const routes: Routes = [
  {
    path: '',
    component: RunBookPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RunBookRoutingModule { }