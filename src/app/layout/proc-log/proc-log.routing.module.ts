import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProcLogPage } from './proc-log/proc-log.page';

const routes: Routes = [
  {
    path: '',
    component: ProcLogPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProcLogRoutingModule { }