import { HttpClient } from '@angular/common/http/http';
import { Subscription, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { take } from 'rxjs/operators';
import { SecurityConstants } from '../../models/security-constants.model';

export class HttpConnectionBuilder<T> {
    private url: string = '';
    private endPoint: string = '';

    private parameters: any = [];

    private handlerSucess: (value: T) => void;
    private handlerError: (value: any) => void;

    constructor(protected http: HttpClient) {
    }

    public buildPost(): Subscription {
        return this.http.post(this.createURL(), this.parameters)
        .pipe(take(1))
        .subscribe(
            (res: any) => this.doSuccess(res),
            err => this.doError(err)
        );
    }

    public buildPut(): Subscription {
        return this.http.put(this.createURL(), this.parameters)
        .pipe(take(1))
        .subscribe(
            (res: any) => this.doSuccess(res),
            err => this.doError(err)
        );
    }

    public buildGet(): Subscription {
        return this.http.get(this.createURL() + this.createQueryString())
        .pipe(take(1))
        .subscribe(
            (res: any) => this.doSuccess(res),
            err => this.doError(err)
        );
    }

    public buildGetResolve(): Observable<any> {
        return this.http.get(this.createURL() + this.createQueryString()).pipe(take(1));
    }

    addEndPoint(endPoint: string): HttpConnectionBuilder<T> {
        this.endPoint += endPoint;
        return this;
    }

    addParameter(parameters: any): HttpConnectionBuilder<T> {
        this.parameters = parameters;
        return this;
    }

    private createQueryString(): string {
        const params = new URLSearchParams();

        for (let key in this.parameters) {
            if (this.parameters.hasOwnProperty(key)) {
                if (this.parameters[key]) {
                    params.set(key, this.parameters[key]);
                }
            }
        }

        const queryString = params.toString();

        if (queryString) {
            return '?' + queryString;
        }

        return '';
    }

    private doSuccess(response: T) {
        if (this.handlerSucess) {
            this.handlerSucess(<T>response);
        }
    }

    private doError(error: any) {
        if (this.handlerError) {
            this.handlerError(error);
        }
    }

    addHandlerSucess(handlerSucess: (value: T) => void): HttpConnectionBuilder<T> {
        this.handlerSucess = handlerSucess;
        return this;
    }

    addHandlerError(handlerError: (value: any) => void): HttpConnectionBuilder<T> {
        this.handlerError = handlerError;
        return this;
    }

    private createURL(): string {
        if (this.url === '') {
            this.addApplicationServerDomain();
        }

        return this.url + this.endPoint;
    }

    private addApplicationServerDomain() {
        this.url += environment.applicationURL;
    }
}